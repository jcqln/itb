from openerp import models, fields, api, exceptions



class Curriculum(models.Model):
    _name = 'itb.academic_curriculum'
    
    name = fields.Char(index=True,required=True)
    catalog_ids = fields.Many2many('itb.academic_catalog',relation='itb_academic_curriculum_catalog_rel',string='Catalogs')
