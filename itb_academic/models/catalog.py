from openerp import models, fields, api, exceptions


class Catalog(models.Model):
    _name = 'itb.academic_catalog'
    
    name = fields.Char(index=True,required=True)
    code = fields.Char(index=True)
    credit = fields.Integer()
    note = fields.Text(index=True)
    season = fields.Char(index=True)
    year = fields.Integer(index=True,default=2)
    active = fields.Boolean(default=True)
    program_id = fields.Many2one('itb.academic_program',index=True,required=True,string='Program')   
    research_id = fields.Many2one('itb.hr_research_group',index=True,required=True,string='Research Group')
    curriculum_ids = fields.Many2many('itb.academic_curriculum',relation='itb_academic_curriculum_catalog_rel',string='Curriculum')
    topic_ids = fields.One2many('itb.academic_topic','catalog_id',string='Topics')
    catalog_outcome_ids = fields.One2many('itb.academic_catalog_outcome','catalog_id',string='Outcomes')
    evaluation_ids = fields.One2many('itb.academic_evaluation','catalog_id',string='Evaluations')
    offering_ids = fields.Many2many('itb.academic_offering',relation='itb_academic_offering_catalog_rel',string='Offering')


class Catalog_Outcome(models.Model):
    _name = 'itb.academic_catalog_outcome'
    _rec_name = 'outcome_id'
    
    intensity = fields.Selection([('low','Low'),('medium','Medium'),('high','High')],'Intensity',default='medium')
    active = fields.Boolean(default=True)
    catalog_id = fields.Many2one('itb.academic_catalog',index=True,required=True,ondelete='cascade',string='Catalog')
    outcome_id = fields.Many2one('itb.academic_outcome',index=True,required=True,ondelete='cascade',string='Outcome')
