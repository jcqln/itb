from openerp import models, fields, api, exceptions
from datetime import date


class Thesis(models.Model):
    _name = 'itb.academic_thesis'
    
    name = fields.Char(required=True,index=True)
    note = fields.Text(index=True)
    state = fields.Selection([('draft','Draft'),('confirm','Confirmed'),('go','On Going'),('cancel','Cancel'),('done','Done')])
    progress = fields.Float()
    start = fields.Date()
    finish = fields.Date()
    grade = fields.Selection([('a','A'),('ab','AB'),('b','B'),('bc','BC'),('c','C'),('d','D'),('e','E'),('t','T')])
    active = fields.Boolean(default=True)
    structure_id = fields.Many2one('itb.academic_structure',domain="[('parent_id','=',False)]",index=True,required=True,string='Structure')        
    author_ids = fields.One2many('itb.academic_author','thesis_id',string='Authors')
    req_supervisor_ids = fields.One2many('itb.academic_req_supervisor','thesis_id',string='Requested Supervisors')
    supervisor_ids = fields.One2many('itb.academic_supervisor','thesis_id',string='Supervisors')
    meetup_ids = fields.One2many('itb.academic_meetup','thesis_id',string='Meetups')
    defence_ids = fields.One2many('itb.academic_defence','thesis_id',string='Defence')
    progress_ids = fields.One2many('itb.academic_progress','thesis_id',string='Progress')  
        
        
class Author(models.Model):
    _name = 'itb.academic_author'
    _rec_name = 'partner_id'
    
    partner_id = fields.Many2one('res.partner',index=True,required=True,ondelete='cascade',string='Person')
    thesis_id = fields.Many2one('itb.academic_thesis',index=True,required=True,ondelete='cascade',string='Thesis')
    sequence = fields.Integer()
    graduation_ids = fields.Many2many('itb.academic_graduation',relation='itb_academic_graduation_author',readonly=True)


class Requested_Supervisor(models.Model):
    _name = 'itb.academic_req_supervisor'
    _rec_name = 'partner_id'
    
    partner_id = fields.Many2one('res.partner',index=True,required=True,ondelete='cascade',string='Person')
    thesis_id = fields.Many2one('itb.academic_thesis',index=True,required=True,ondelete='cascade',string='Thesis')
    sequence = fields.Integer()
    
    
class Supervisor(models.Model):
    _name = 'itb.academic_supervisor'
    _rec_name = 'partner_id'
    
    partner_id = fields.Many2one('res.partner',index=True,required=True,ondelete='cascade',string='Person')
    thesis_id = fields.Many2one('itb.academic_thesis',index=True,required=True,ondelete='cascade',string='Thesis')
    sequence = fields.Integer()