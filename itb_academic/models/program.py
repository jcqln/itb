from openerp import models, fields, api, exceptions


class Program(models.Model):
    _name = 'itb.academic_program'
    
    name = fields.Char(index=True,required=True)
    prefix = fields.Char()
    degree = fields.Selection([('d3','Diploma'),('s1','Undergraduate'),('s2','Master'),('s3','Doctoral'),('non','Non Degree')],'Degree',default='s1')
    active = fields.Boolean(default=True)
    catalog_ids = fields.One2many('itb.academic_catalog','program_id',string='Catalogs')
    outcome_ids = fields.Many2many('itb.academic_outcome',relation='itb_academic_program_outcome_rel',string='Outcomes')    
