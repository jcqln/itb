from openerp import models, fields, api, exceptions



class Season(models.Model):
    _name = 'itb.academic_season'
    
    name = fields.Char(index=True)
    type = fields.Selection([('even','Even'),('odd','Odd'),('short','Short')])
    year = fields.Integer()
    start = fields.Date(required=True)
    finish = fields.Date(required=True)
