[STEI](http://stei.itb.ac.id/)
---
**Powered by [Odoo 8](https://www.odoo.com/)**

# How to commit
Perintah `git commit` digunakan untuk menyatakan bahwa file yang ada di *working directory* siap dinyatakan sebuah status.

## Prepare commit
Sebelum melakukan **commit**, terlebih dahulu harus menambahkan file-file yang berubah ke *working directory* dengan cara memanggil perintah `git add --all`. Untuk memastikan, pastikan pemanggilan perintah tersebut berada di root repository ini (untuk project ini berada di dalam folder `itb`).

Untuk melihat file-file apa saja yang sudah masuk ke *working directory*, dapat dilakukan dengan perintah `git status`.

## Commit
Setelah menambahkan file-file yang akan dilakukan commit ke *working directory*, panggil dengan perintah `git commit -m \"<message>\"`. `<message>` merupakan field untuk mengisi pesan dari `commit` (tentu tanpa karakter kurang dari dan lebih dari).

# How to push and pull
**PUSH** merupakan perintah untuk mengirim commit yang ada di lokal ke *remote*. Untuk melakukannya biasanya menggunakan perintah `git push origin master`. `origin` merupakan nama cabang yang sedang aktif di lokal, sedangkan `master` merupakan nama cabang utama baik di lokal maupun di *remote*. **PERHATIAN:** Perintah ini akan meminta otentikasi akun git repository (dalam hal ini [bitbucket](http://bitbucket.org/)), maka jangan lupa untuk memasukkan *username* dan *password* untuk login ke [bitbucket](http://bitbucket.org).

**PULL** merupakan perintah untuk mengunduh commit terbaru daripada commit yang ada di lokal. Untuk melakukannya cukup dengan menggunakan perintah `git pull`. Perintah ini akan mengunduh daftar perubahan pada repository. **PERHATIAN:** Karena repository ini **private**, maka pada saat pemanggilan perintah ini akan meminta nama pengguna dan kata kunci seperti pada saat akan push. **SANGAT PERLU DIPERHATIKAN:** Pastikan perubahan terbaru yang ada di *remote* dengan yang ada di lokal tidak ada di baris yang sama atau lebih mudanya merubah di file yang sama. Hal ini memungkinkan terjadinya *merge conflict* yang artinya ada file yang konflik dan perlu dipilih baris yang diterima. Jika hal ini terjadi, harap hubungi pemilik repository ini.

# Iterasi Coding di Odoo
Inilah urutan checklist cara coding di odoo supaya fitur framework odoo bisa termanfaatkan maksimal (less code).
Urutan ini diberlakukan di setiap use-case. jadi checklist ini bisa berdampak ke beberapa object model yang terlibat 
dalam use-case itu. Urutan checklist yang wajib dilakukan hanya yang kategori Basic. Sisanya disesuaikan dengan kebetuhan 
use-case nya.

## Basic
	* menu
	* action window
	* Basic/relational field
	* field
	* form/tree view

## Validation
	* default
	* compute/depends
	* constraint
	* relational domain
	
## View
	* onchange
	* search
	* filter
	* group
	* graph view
	* optional: kanban/gant/calendar view
	* smart button
	* widget
	* context action

## Security
	* access control
	* record rule
	* field group/state

## wizard

## workflow

## Server Action

## Report

## Dashboard

## Website
