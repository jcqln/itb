from openerp import models, fields, api, exceptions


import logging
_logger = logging.getLogger(__name__)	
	
class Order_Wizard(models.TransientModel):
	_name = 'itb.plan_order_wizard'
	'''
	def _get_request_ids(self):
		return self.env['itb.plan_payment'].browse(self.env.context.get('active_ids',[]))
	request_ids = fields.One2many('itb.plan_payment','payment_order_id','Related Payment Requests',default=_get_request_ids)
	'''
	order_id = fields.Many2one('itb.plan_order', domain="[('state','=','draft')]", required=True,string='Payment Order')
		
	@api.multi
	def set_order_id(self):
		self.ensure_one()
		if not (self.order_id):
			raise exceptions.ValidationError('No Payment Order selected!')
		#import pdb; pdb.set_trace()
		#_logger.debug('Selected Payment Requests %s',self.request_ids.ids)
		#requests = self.env['itb.plan_payment'].browse(self.env.context.get('active_ids',[]))
		ids = self.env.context.get('active_ids',[])
		requests = self.env['itb.plan_request'].search([('state','=','validate'),('id','in',ids)])
		requests.write({'order_id':self.order_id.id})
		return {}
