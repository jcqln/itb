from openerp import models, fields, api, exceptions
from datetime import date


class Implementation(models.Model):
	_name = 'itb.plan_implementation'
	_rec_name = 'plan_id'
	
	quarter = fields.Selection([('q1','Jan-Mar'),('q2', 'Apr-Jun'),('q3','Jul-Sep'),('q4','Oct-Dec')], 'Quarter',default='q1')
	reference = fields.Char()
	state = fields.Selection([('draft','Draft'),('confirm','Confirmed'),('validate','Validated')], 'Status', default='draft')
	plan_id = fields.Many2one('itb.plan',ondelete='cascade',required=True,index=True,domain="[('state','=','validate')]")
	spending_actual_ids = fields.One2many('itb.plan_spending_actual', 'implementation_id', 'Spending Implementation Lines', copy=True)
	
	def action_budget_state_confirmed(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'confirm' })
		return True

	def action_budget_state_validated(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'validate' })
		return True

	def action_budget_state_abort(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'draft' })
		return True