from openerp import models, fields, api, exceptions
from datetime import date


class Receipt(models.Model):
	_name = 'itb.plan_receipt'
	_rec_name = 'reference'
	
	reference = fields.Char(required=True)
	day = fields.Date()
	day_transfer = fields.Date()
	total = fields.Float()
	note = fields.Text()
	plan_id = fields.Many2one('itb.plan',ondelete='cascade',required=True,index=True,domain="[('state','=','validate')]")
	receipt_order_ids = fields.One2many('itb.plan_receipt_order', 'receipt_id', 'Order')

	

class Receipt_Order(models.Model):
	_name = 'itb.plan_receipt_order'
	
	receipt_id = fields.Many2one('itb.plan_receipt',ondelete='cascade',required=True,index=True)
	order_id = fields.Many2one('itb.plan_order',ondelete='cascade',required=True,index=True)
	reference = fields.Char(related='order_id.name')
	total = fields.Float(related='order_id.total')
	
	@api.multi
	def write(self, values, context=None):
		self.write(values, context=context)
