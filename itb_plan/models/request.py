from openerp import models, fields, api, exceptions
from datetime import date


class Request(models.Model):
	_name = 'itb.plan_request'
	_rec_name = 'pay_to'
	
	reference = fields.Char()
	day = fields.Date()
	due = fields.Date()
	total = fields.Float(default=0)
	#is_advance = fields.Boolean()
	is_reconciled = fields.Boolean()
	note = fields.Text()
	type = fields.Selection([('barang','Barang'),('pegawai', 'Pegawai'),('jasa','Jasa'),('modal','Modal')], 'Type', default='jasa', required=True)
	total_budget = fields.Float(default=0, compute='_total_budget', store=True, readonly=True)

	state = fields.Selection([('draft','Draft'),('confirm','Confirmed'),('validate','Validated'),('paid','Paid')], 'Status', default='draft',select=True, required=True, readonly=True, copy=False)
	#unit_id = fields.Many2one('itb.plan_unit',ondelete='cascade',required=True,index=True)
	plan_id = fields.Many2one('itb.plan',ondelete='cascade',required=True,index=True,domain="[('state','=','validate')]")
	pay_to = fields.Many2one('res.partner',ondelete='cascade',required=True,index=True)
	request_line_ids = fields.One2many('itb.plan_request_line', 'request_id', 'Request Line', copy=True)
	order_id = fields.Many2one('itb.plan_order',ondelete='cascade',index=True)
	
	@api.model
	def action_payment_req_confirmed(self):
		# for budget in self.env['itb.plan_payment_budget'].search('payment_id','=',self.id):
		# 	budget.used = budget.total
		for budget in self.request_line_ids:
			budget.used = budget.total
			budget.spending_actual_id._recalculate_all_used()
			budget.spending_actual_id.spending_id._sum_used()
			budget.spending_actual_id.spending_id.plan_line_id._compute_spending()
			budget.spending_actual_id.spending_id.plan_id._compute_avg()
		self.write({ 'state' : 'confirm' })
		return True

	def action_payment_req_paid(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'paid' })
		return True

	# def action_payment_req_abort(self, cr, uid, ids):
	@api.model
	def action_payment_req_abort(self):
		# for budget in self.env['itb.plan_payment_budget'].search('payment_id','=',self.id):
		# 	budget.used = 0
		for budget in self.request_line_ids:
			budget.used = 0
			budget.spending_actual_id._recalculate_all_used()
			budget.spending_actual_id.spending_id._sum_used()
			budget.spending_actual_id.spending_id.plan_line_id._compute_spending()
			budget.spending_actual_id.spending_id.plan_id._compute_avg()
		self.write({ 'state' : 'draft' })
		return True

	# def _trigger_recalculate(self,cr,uid,ids):
	# 	for spending in env['itb.plan_spending_actual']:
	# 		spending._recalculate_used()

	@api.one
	@api.depends('request_line_ids')
	def _total_budget(self):
		# totals = [total.total for total in self.payment_budget_ids]
		self.total_budget = 0
		for payment in self.request_line_ids:
			self.total_budget += payment.total
		# self.budget = sum(totals)

	def _check_below_zero(self, cr, uid, ids, context=None):
		for budget in self.browse(cr,uid,ids,context=context):
			if budget.total < 0.01:
				return False
		return True

	def _check_total_below(self, cr, uid, ids, context=None):
		for budget in self.browse(cr,uid,ids,context=context):
			if budget.total < budget.total_budget:
				return False
		return True

	def _check_total_equal(self, cr, uid, ids, context=None):
		for budget in self.browse(cr,uid,ids,context=context):
			if budget.total != budget.total_budget:
				return False
		return True

	_constraints = [
		(_check_below_zero,'Total payment is below or equal zero.',['total']),
		# (_check_total_below,'Total payment budget is below than sum of payment budget line.',['total']),
		#(_check_total_equal,'Total payment is not equal with sum of payment budget.',['total'])
	]




class Request_Line(models.Model):
	_name = 'itb.plan_request_line'

	request_id = fields.Many2one('itb.plan_request',ondelete='cascade',required=True,index=True)
	spending_actual_id = fields.Many2one('itb.plan_spending_actual',ondelete='cascade',required=True,index=True,domain="[('implementation_id.state','=','validate')]")
	initiative = fields.Char(related='spending_actual_id.plan_line_id.name')
	type = fields.Selection(related='spending_actual_id.type')
	used = fields.Float(default=0)
	
	def _available_budget(self):
		budget = self.env['itb.plan_spending_actual'].browse(self.spending_actual_id.id)
		return float(self.spending_actual_id.id)
		
	# total_original = fields.Float(related='spending_actual_id.total')
	total = fields.Float(default=0)


	def _check_type(self, cr, uid, ids, context=None):
		for budget in self.browse(cr,uid,ids,context=context):
			for budgets in budget.request_id.request_line_ids:
				if budget.request_id.type != budgets.type:
					return False
		return True

	def _check_below_zero(self, cr, uid, ids, context=None):
		for budget in self.browse(cr,uid,ids,context=context):
			if budget.total < 0.01:
				return False
		return True

	def _check_total(self, cr, uid, ids, context=None):
		for budget in self.browse(cr,uid,ids,context=context):
			if budget.total > (budget.spending_actual_id.total - budget.spending_actual_id.used):
				return False
		return True

	# @api.one
	# @api.onchange('used')
	# def _onchange_used(self):
	# 	for spending in self.env['itb.plan_spending_actual'].browse([self.spending_actual_id]):
	# 		spending._recalculate_all_used()

	_constraints = [
		(_check_below_zero,'Total payment budget is below or equal zero.',['total']),
		(_check_total,'Total payment budget is over than usable budget.',['total']),
		(_check_type,'There is one and more payment budget type not equal with payment type.',['spending_actual_id'])
	]