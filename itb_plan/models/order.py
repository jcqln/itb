from openerp import models, fields, api, exceptions
from datetime import date


class Order(models.Model):
	_name = 'itb.plan_order'

	name = fields.Char(string='Reference',required=True)
	day = fields.Date(required=True)
	total = fields.Float(compute='calculate_total_budget',store=True)
	state = fields.Selection([('draft','Draft'),('confirm','Confirmed'),('paid','Paid')], 'Status', default='draft',select=True, required=True, readonly=True, copy=False)
	request_ids = fields.One2many('itb.plan_request', 'order_id', 'Requests',copy=False)

	@api.one
	@api.depends('request_ids')
	def calculate_total_budget(self):
		self.total = 0
		for payment in self.request_ids:
			self.total += payment.total_budget
