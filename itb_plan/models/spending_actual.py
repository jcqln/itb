from openerp import models, fields, api, exceptions
from datetime import date


class Spending_Actual(models.Model):
	_name = 'itb.plan_spending_actual'
	
	name = fields.Char(related='spending_id.name')
	code = fields.Char(related='spending_id.code', readonly=True)
	month = fields.Selection([('Jan','Jan'),('Feb', 'Feb'),('Mar','Mar'),('Apr','Apr'),('May','May'),('Jun','Jun'),('Jul','Jul'),('Aug','Aug'),('Sep','Sep'),('Oct','Oct'),('Nov','Nov'),('Dec','Dec')], 'Month', default='Jan', required=True, related='spending_id.month',store=True)
	day = fields.Date(related='spending_id.day',store=True)
	standard = fields.Char(related='spending_id.standard',readonly=True)
	price = fields.Float(related='spending_id.price',default=0)
	volume = fields.Float(related='spending_id.volume',default=0)
	total = fields.Float(compute='_calculate_total', store=True, readonly=True,default=0)
	used = fields.Float(readonly=True,default=0)
	available = fields.Float(compute='_sum_available', store=True,readonly=True)
	paid = fields.Float(readonly=True,default=0)
	percent_budget = fields.Float(compute='_recalculate_percent_budget',store=True,readonly=True,default=0)
	active = fields.Boolean(default=True)
	
	spending_id = fields.Many2one('itb.plan_spending',ondelete='cascade',required=True,index=True)
	type = fields.Selection([('barang','Barang'),('pegawai', 'Pegawai'),('jasa','Jasa'),('modal','Modal')], 'Type', related='spending_id.type', readonly=True, store=True)
	source = fields.Selection([('dm','Dana Masyarakat'),('boptn', 'BOPTN')], 'Source', related='spending_id.source', readonly=True)
	price_id = fields.Many2one('itb.plan_price',related='spending_id.price_id',ondelete='cascade',required=True,index=True)
	plan_line_id = fields.Many2one('itb.plan_line',related='spending_id.plan_line_id',ondelete='cascade',index=True, readonly=True, store=True)
	plan_id = fields.Many2one('itb.plan',related='spending_id.plan_id',ondelete='cascade',index=True,store=True)
	implementation_id = fields.Many2one('itb.plan_implementation',ondelete='cascade',required=True,index=True)
	confirmation_id = fields.Many2one('itb.plan_confirmation',ondelete='cascade',index=True)
	request_line_ids = fields.One2many('itb.plan_request_line', 'spending_actual_id', 'Request Line', index=True)
	user_ids = fields.Many2many('res.users')

	@api.one
	@api.depends('price')
	def _calculate_total(self):
		self.total = self.price * self.volume

	# @api.one
	# @api.depends('payment_budget_ids')
	# @api.onchange('payment_budget_ids')
	# def _recalculate_used(self):
	# 	self.used = sum([budget.used for budget in self.payment_budget_ids])
		# self.used = 0
		# for payment_budget in self.payment_budget_ids:
		# 	pay_ment = self.env['itb.plan_payment'].browse(payment_budget.payment_id)
		# 	if pay_ment.search([('state','<>','draft')]):
		# 		self.used += payment_budget.total
			# if pay_ment.state != 'draft':
			# 	self.used += payment_budget.total
		# self.used = sum([payment.total for payment in self.payment_budget_ids])

	@api.one
	def _recalculate_all_used(self):
		self.used = sum([budget.used for budget in self.request_line_ids])

	@api.one
	@api.depends('total','used')
	def _recalculate_percent_budget(self):
		if self.total > 0:
			self.percent_budget = 100*self.used / self.total
		else:
			self.percent_budget = 0

	@api.depends('total','used')
	def _sum_available(self):
		for record in self:
			record.available = record.total - record.used
			
	# def write(self, cr, uid, ids, vals, context=None):
	# 	self.spending_id._sum_used()
	# 	res = super(my_class, self).write(cr, uid, ids, vals, context=context)
	# 	return res