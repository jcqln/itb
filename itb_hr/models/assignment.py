from openerp import models, fields, api, exceptions

class Assignment(models.Model):
	_name = 'itb.hr_assignment'
	_rec_name = 'job_id'
	
	reference = fields.Char()
	start = fields.Date()
	finish = fields.Date()
	
	job_id =  fields.Many2one('hr.job', string='Job Title')
	department_id =  fields.Many2one('hr.department', string='Department')
	employee_id =  fields.Many2one('hr.employee', string='Name')