from openerp import models, fields, api, exceptions

class Holiday(models.Model):
	_name = 'itb.holiday'
		
	employee_id =  fields.Many2one('hr.employee', string='Name',index=True)
	name = fields.Char()
	day = fields.Date()
	is_national = fields.Boolean()
