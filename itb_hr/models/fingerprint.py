from openerp import models, fields, api, exceptions
from datetime import datetime, timedelta

class Fingerprint(models.Model):
	_name = 'itb.hr_fingerprint'
	_rec_name = 'fingerprint_id'
	_order = 'time asc'
		
	fingerprint_id =  fields.Char(index=True)
	time = fields.Datetime(index=True)

	@api.one
	@api.constrains('fingerprint_id','time')
	def _not_same_fingeprint_same_person(self):
		current_fingerprints_thisperson = self.env['itb.hr_fingerprint'].search([('fingerprint_id','=',self.fingerprint_id)])
		counter = 1
		for fingerprint in current_fingerprints_thisperson:
			if (counter < len(current_fingerprints_thisperson)):
				format = '%Y-%m-%d %H:%M:%S'
				datetime_fingerprint = datetime.strptime(fingerprint.time,format)
				datetime_checked_fingerprint = datetime.strptime(self.time,format)
				if (datetime_fingerprint.date() == datetime_checked_fingerprint.date()):
					if (datetime_fingerprint.time() == datetime_checked_fingerprint.time()):
						raise exceptions.ValidationError('Same fingerprint for certain employee(s) should not be resubmitted!')
			counter = counter + 1

	@api.one
	@api.constrains('fingerprint_id','time')
	def _update_attendance(self):
		format = '%Y-%m-%d %H:%M:%S'
		datetime_fingerprint = datetime.strptime(self.time,format) - timedelta(hours=7)
		employee_this_person = self.env['hr.employee'].search([('finger_id', '=', self.fingerprint_id)])
		attendance_thisperson = self.env['itb.hr_attendance'].search(['&', ('employee_id.id', '=', employee_this_person.id), ('day', '=', datetime_fingerprint.date())])
		if(attendance_thisperson.is_attend == False):
			attendance_thisperson.write({'actual_in_time':datetime_fingerprint,'actual_out_time':datetime_fingerprint,'is_attend':True})
		else:
			if(self.time < attendance_thisperson.actual_in_time):
				attendance_thisperson.write({'actual_in_time':datetime_fingerprint})
			if(self.time > attendance_thisperson.actual_out_time):
				attendance_thisperson.write({'actual_out_time':datetime_fingerprint})