from openerp import models, fields, api, exceptions

class ShiftAllocation(models.Model):
	_name = 'itb.hr_shift_allocation'
	_rec_name = 'employee_id'
		
	employee_id =  fields.Many2one('hr.employee', string='Name',index=True)
	shift_id =  fields.Many2one('itb.hr_shift', string='Shift',index=True)
	start = fields.Date()
	finish = fields.Date()
	is_active = fields.Boolean()

	@api.one
	@api.constrains('start','finish')
	def _not_intercept_schedule_one_employee(self):
		shiftAllocationThisEmployee = self.env['itb.hr_shift_allocation'].search([('employee_id','=',self.employee_id.id)])
		shiftAllocationThisEmployeeCount = self.env['itb.hr_shift_allocation'].search_count([('employee_id','=',self.employee_id.id)])
		counter = 1
		for shiftAllocation in shiftAllocationThisEmployee:
			if counter < shiftAllocationThisEmployeeCount:
				if (self.start <= shiftAllocation.finish) and (self.finish >= shiftAllocation.start):
					raise exceptions.ValidationError('The employee already have shift allocated on this date range!')
			counter = counter + 1

	@api.one
	@api.constrains('start','finish')
	def _start_date_no_more_then_finish_date(self):
		if self.start > self.finish:
			raise exceptions.ValidationError('finish date should be greather equal then start date')