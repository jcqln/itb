from openerp import models, fields, api, exceptions
from datetime import datetime

class Leave(models.Model):
	_name = 'itb.hr_leave'
		
	def _set_current_employee(self):
		return self.env['hr.employee'].search([('user_id','=',self.env.user.id)])

	employee_id =  fields.Many2one('hr.employee', string='Name',index=True,default=_set_current_employee)
	type =  fields.Many2one('itb.hr_leave_type', index=True)
	note = fields.Char()
	start = fields.Date()
	finish = fields.Date()
	state = fields.Selection([('draft','Draft'),('confirmed','Confirmed'),('valid','Validated')], 'Status', default='draft',select=True, required=True, readonly=True, copy=False)
	# should_approved = fields.Boolean(compute='_set_approved_this_record', search='_search_approved_this_record')

	# @api.one
	# @api.depends('employee_id')
	# def _set_approved_this_record(self):
	# 	hr_employee = self.env['hr.employee']
	# 	this_employee = hr_employee.search([('user_id','=',self.env.user.id)])

	# 	mapping_approved = self.env['itb.hr_approval_records']
	# 	mapping_records_counts = mapping_approved.search_count([('applicant_id','=',self.employee_id.id),('approver_id','=',this_employee[-1].id),('model','=','leave'),('object_id','=',self.id)]) 
	# 	mapping_records_employee = mapping_approved.search([('applicant_id','=',self.employee_id.id),('approver_id','=',this_employee[-1].id),('model','=','leave'),('object_id','=',self.id)])
		
	# 	self.should_approved = False
	# 	if mapping_records_counts > 0:
	# 		if mapping_records_employee[-1].sequence > 1:
	# 			previous_sequence_approver = mapping_records_employee[-1].sequence - 1
	# 			mapping_records_previous = mapping_approved.search([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id),('sequence','=',previous_sequence_approver)])
	# 			if mapping_records_previous[-1].is_approved == "approved" and mapping_records_employee[-1].is_approved == "pending":
	# 				self.should_approved = True
	# 		else:
	# 			if mapping_records_employee[-1].is_approved == "pending":
	# 				self.should_approved = True

	# def _search_approved_this_record(self,operator,value):
	# 	hr_employee = self.env['hr.employee']
	# 	this_employee = hr_employee.search([('user_id','=',self.env.user.id)])

	# 	mapping_approved = self.env['itb.hr_approval_records']
	# 	mapping_records_employee = mapping_approved.search([('approver_id','=',this_employee[-1].id),('model','=','leave'),('is_approved','=','pending')])
	# 	mapping_records_counts = mapping_approved.search_count([('approver_id','=',this_employee[-1].id),('model','=','leave'),('is_approved','=','pending')]) 

	# 	applicant_records = []
	# 	if mapping_records_counts > 0:
	# 		for request_travel in mapping_records_employee:
	# 			if request_travel.sequence > 1:
	# 				previous_sequence_approver = request_travel.sequence - 1
	# 				mapping_records_previous = mapping_approved.search([('model','=','leave'),('object_id','=',request_travel.object_id.id),('sequence','=',previous_sequence_approver)])
	# 				if mapping_records_previous[-1].is_approved == "approved" and request_travel.is_approved == "pending":
	# 					#raise exceptions.ValidationError(request_travel.applicant_id.name)
	# 					applicant_records.append(request_travel.object_id)
	# 			else:
	# 				if request_travel.is_approved == "pending":
	# 					#raise exceptions.ValidationError(request_travel.applicant_id.name)
	# 					applicant_records.append(request_travel.object_id)

	# 	#raise exceptions.ValidationError(self.employee_id.id)
	# 	return [('id','in',applicant_records)]

	@api.one
	@api.constrains('start','finish')
	def _start_date_no_more_then_finish_date(self):
		if self.start > self.finish:
			raise exceptions.ValidationError('finish date should be greather equal then start date')

	@api.one
	@api.constrains('start','finish')
	def _leave_request_not_intersected(self):
		leave_request_model = self.env['itb.hr_leave']
		leave_request_thisemployee = leave_request_model.search([('employee_id','=',self.employee_id.id),('type','=',self.type.id)])
		leave_request_counts = leave_request_model.search_count([('employee_id','=',self.employee_id.id),('type','=',self.type.id)])
		counter = 1
		for leave in leave_request_thisemployee:
			if counter < leave_request_counts:
				if self.start <= leave.finish and self.finish >= leave.start:
					raise exceptions.ValidationError('The employee has already leave request on this range at this type!')
			counter = counter + 1

	@api.one
	@api.constrains('start','finish','type')
	def _is_leave_already_allocated(self):
		leave_alloc_model = self.env['itb.hr_leave_allocation']
		leave_alloc_thisemployee = leave_alloc_model.search([('leave_type','=',self.type.id),('employee_id','=',self.employee_id.id)])
		
		already_allocated = False
		start_converted = datetime.strptime(self.start,'%Y-%m-%d')
		finish_converted = datetime.strptime(self.finish,'%Y-%m-%d')
		for leave_alloc in leave_alloc_thisemployee:
			difference_days_used = (finish_converted - start_converted).days + 1
			if leave_alloc.year == start_converted.year and leave_alloc.year == finish_converted.year and difference_days_used <= leave_alloc.day_saldo:
				already_allocated = True

		if already_allocated == False:
			raise exceptions.ValidationError('The Employee has not allocated this leave type on this year OR allocation saldo is not enough!')

	@api.multi
	def action_state_draft(self):
		self.state = 'draft'
		# if self.state == 'confirmed':
		# 	hr_employee = self.env['hr.employee']
		# 	this_employee = hr_employee.search([('user_id','=',self.env.user.id)])

		# 	apprecords_model = self.env['itb.hr_approval_records']
		# 	apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('approver_id','=',this_employee[-1].id),('model','=','leave'),('object_id','=',self.id)])
			
		# 	if len(apprecords_thisemployee) > 0:
		# 		if apprecords_thisemployee[-1].sequence > 1:
		# 			previous_sequence_approver = apprecords_thisemployee[-1].sequence - 1
		# 			apprecords_previous = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id),('sequence','=',previous_sequence_approver)])
		# 			if apprecords_thisemployee[-1].is_approved == "pending" and apprecords_previous[-1].is_approved == "approved":
		# 				all_apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)])
		# 				for apprecords in all_apprecords_thisemployee:
		# 					apprecords.write({'is_approved':'rejected'})
		# 				self.state = 'draft'
		# 			else:
		# 				raise exceptions.ValidationError('You are not authorized to abort this request!')
		# 		else:
		# 			if apprecords_thisemployee[-1].is_approved == "pending":
		# 				all_apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)])
		# 				for apprecords in all_apprecords_thisemployee:
		# 					apprecords.write({'is_approved':'rejected'})
		# 				self.state = 'draft'
		# 			else:
		# 				raise exceptions.ValidationError('You have already approved this request!')
		# 	else:
		# 		raise exceptions.ValidationError('You are not authorized to abort this request!')
		# else:
		# 	self.state = 'draft'

	@api.multi
	def action_state_confirm(self):
		self.state = 'confirmed'
		# apprecords_model = self.env['itb.hr_approval_records']
		# apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)]) 
		# apprecords_thisemployee_count = apprecords_model.search_count([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)]) 
		
		# # Cari trial request terakhir (apakah baru atau sudah ada)
		# latestTrial = 0
		# if apprecords_thisemployee_count > 0:
		# 	for apprecords in apprecords_thisemployee:
		# 		if (apprecords.trial > latestTrial):
		# 			latestTrial = apprecords.trial

		# # Tulis approval di daftar approval record dengan trial terbaru
		# if self.employee_id.user_id == self.env.user:
		# 	appmapping_model = self.env['itb.hr_request_approver']
		# 	approval_lists = appmapping_model.search([('applicant_id','=',self.employee_id.id)])
		# 	# Cari template email
		# 	# template_id = self.pool.get('email.template').search(self.env.cr, self.env.uid, [("name","=","Notifikasi Request Cuti")])
		# 	for approval in approval_lists:
		# 		apprecords_model.create({'applicant_id':self.employee_id.id,'approver_id':approval.approver_id.id,'sequence':approval.sequence,'model':'leave','is_approved':'pending','object_id':self.id,'trial':latestTrial+1})
		# 		# Proses pengiriman email
		# 		# if template_id:
		# 		# 	values = self.pool.get('email.template').generate_email(self.env.cr, self.env.uid, template_id[0], self.id, context=None)
		# 		# 	values['email_to'] = approval.approver_id.work_email
		# 		# 	mail_mail_obj = self.pool.get('mail.mail')
		# 		# 	msg_id = mail_mail_obj.create(self.env.cr, self.env.uid, values, context=None)
		# 		# 	if msg_id:
		# 		# 		mail_mail_obj.send(self.env.cr, self.env.uid, [msg_id], context=None)
		# 	self.state = 'confirmed'
		# else:
		# 	raise exceptions.ValidationError('You are not authorized to confirm this request!')

	@api.multi
	def action_state_valid(self):
		# self.state = 'valid'
		# apprecords_model = self.env['itb.hr_approval_records']
		# apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)]) 
		# apprecords_counts = apprecords_model.search_count([('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)]) 
		
		# # Cari Di Mana Urutan Approver Saat Ini
		# sequence_approver_thisemployee = None
		# for apprecords in apprecords_thisemployee:
		# 	if apprecords.approver_id.user_id.id == self.env.user.id:
		# 		sequence_approver_thisemployee = apprecords.sequence

		# # Telusuri Approver Sebelumnya dan Cek Statusnya
		# approve_complete = True
		# if sequence_approver_thisemployee != None:
		# 	if sequence_approver_thisemployee > 1:
		# 		previous_sequence_approver = sequence_approver_thisemployee - 1
		# 		previous_approver_thisemployee = apprecords_model.search([('sequence','=',previous_sequence_approver),('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)])
		# 		if previous_approver_thisemployee[-1].is_approved == 'pending':
		# 			approve_complete = False
		# 			raise exceptions.ValidationError('You are not authorized to validate the leave request!')
		# 		else:
		# 			this_approver_apprecords = apprecords_model.search([('sequence','=',sequence_approver_thisemployee),('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)])
		# 			this_approver_apprecords[-1].write({'is_approved':'approved'})
		# 	else:
		# 		if apprecords_counts > 1:
		# 			approve_complete = False
		# 		this_approver_apprecords = apprecords_model.search([('sequence','=',sequence_approver_thisemployee),('applicant_id','=',self.employee_id.id),('model','=','leave'),('object_id','=',self.id)])
		# 		this_approver_apprecords[-1].write({'is_approved':'approved'})
		# else:
		# 	approve_complete = False
		# 	raise exceptions.ValidationError('You are not authorized to validate the leave request!')

		# Jika approval komplit dinyatakan valid (kurangi alokasi hari di leave allocation)
		# if approve_complete != True:
		# 	self.state = 'confirmed'
		# else:
		start_converted = datetime.strptime(self.start,'%Y-%m-%d')
		finish_converted = datetime.strptime(self.finish,'%Y-%m-%d')
		difference_days = (finish_converted - start_converted).days + 1

		leave_alloc_model = self.env['itb.hr_leave_allocation']
		leave_alloc_thisemployee = leave_alloc_model.search([('employee_id','=',self.employee_id.id),('leave_type','=',self.type.id),('year','=',start_converted.year)])
		leave_alloc_thisemployee.write({'day_used':difference_days+leave_alloc_thisemployee[-1].day_used})
		
		self.state = 'valid'