from openerp import models, fields, api, exceptions
import csv
import base64

class HRAttendanceUpload(models.Model):
	_name = 'itb.hr_attendance_upload'

	upload = fields.Binary('File', required=True)
	delimiter = fields.Char('Delimiter')

	# def import_file(self, cr, uid, ids, context=None):
	@api.one
	def import_file(self):
		fileobj = base64.decodestring(self.upload)

		# your treatment
		self.env['itb.hr_fingerprint'].search([]).unlink()
		first_skip = True
		for row in csv.reader(fileobj.splitlines(), delimiter=';'):
			if first_skip:
				first_skip = False
			else:
				self.env['itb.hr_fingerprint'].create({'fingerprint_id':row[0], 'time':row[1]})
			pass
		return True
