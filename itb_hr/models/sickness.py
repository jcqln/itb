from openerp import models, fields, api, exceptions

class Sickness(models.Model):
	_name = 'itb.hr_sickness'
	_order = 'start,finish asc'
		
	employee_id =  fields.Many2one('hr.employee', string='Name',index=True)
	note = fields.Char()
	hospital = fields.Char()
	start = fields.Date()
	finish = fields.Date()