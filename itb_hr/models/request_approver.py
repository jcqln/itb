from openerp import models, fields, api, exceptions

class RequestApprover(models.Model):
	_name = 'itb.hr_request_approver'
	
	applicant_id =  fields.Many2one('hr.employee', string='Applicant')
	approver_id =  fields.Many2one('hr.employee', string='Approver')
	sequence = fields.Integer()