from openerp import models, fields, api, exceptions

class Training(models.Model):
	_name = 'itb.hr_training'

	def _set_current_employee(self):
		return self.env['hr.employee'].search([('user_id','=',self.env.user.id)])
	
	name = fields.Char()
	start = fields.Date()
	finish = fields.Date()
	provider = fields.Char()
	
	role_ids = fields.Many2many('itb.hr_training_role')
	country_id =  fields.Many2one('res.country', string='Country', default='Indonesia')
	state = fields.Selection([('draft','Draft'),('confirmed','Confirmed'),('valid','Validated')], 'Status', default='draft',select=True, required=True, readonly=True, copy=False)
	employee_id =  fields.Many2one('hr.employee', string='Name', default=_set_current_employee, readonly=True)

	@api.one
	@api.constrains('start','finish')
	def _start_date_no_more_then_finish_date(self):
		if self.start > self.finish:
			raise exceptions.ValidationError('finish date should be greather equal then start date')

	@api.multi
	def action_state_draft(self):
		self.state = 'draft'

	@api.multi
	def action_state_confirm(self):
		self.state = 'confirmed'

	@api.multi
	def action_state_valid(self):
		self.state = 'valid'
	
	
class Training_Role(models.Model):
	_name = 'itb.hr_training_role'
	
	name = fields.Char()
	training_ids = fields.Many2many('itb.hr_training')