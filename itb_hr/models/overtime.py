from openerp import models, fields, api, exceptions
from datetime import datetime, timedelta

class Overtime(models.Model):
	_name = 'itb.hr_overtime'
		
	def _set_current_employee(self):
		return self.env['hr.employee'].search([('user_id','=',self.env.user.id)])

	employee_id =  fields.Many2one('hr.employee', string='Name',index=True,default=_set_current_employee)
	note = fields.Char()
	day = fields.Date()
	start1 = fields.Datetime()
	finish1 = fields.Datetime()
	start2 = fields.Datetime()
	finish2 = fields.Datetime()
	duration_minute1 = fields.Float(readonly=True, default=0, store=True, compute='_get_overtime_departure')
	duration_minute2 = fields.Float(readonly=True, default=0, store=True, compute='_get_overtime_arrival')
	state = fields.Selection([('draft','Draft'),('confirmed','Confirmed'),('valid','Validated')], 'Status', default='draft',select=True, required=True, readonly=True, copy=False)
	# should_approved = fields.Boolean(compute='_set_approved_this_record', search='_search_approved_this_record')

	@api.one
	@api.depends('start1','finish1')
	def _get_overtime_departure(self):
		if self.start1 != False and self.finish1 != False:
			converted_start1 = datetime.strptime(self.start1,'%Y-%m-%d %H:%M:%S')
			converted_finish1 = datetime.strptime(self.finish1,'%Y-%m-%d %H:%M:%S')
			self.duration_minute1 = (converted_finish1-converted_start1).total_seconds() // 60

	@api.one
	@api.depends('start2','finish2')
	def _get_overtime_arrival(self):
		if self.start2 != False and self.finish2 != False:
			converted_start2 = datetime.strptime(self.start2,'%Y-%m-%d %H:%M:%S')
			converted_finish2 = datetime.strptime(self.finish2,'%Y-%m-%d %H:%M:%S')
			self.duration_minute2 = (converted_finish2-converted_start2).total_seconds() // 60

	# @api.one
	# @api.depends('employee_id')
	# def _set_approved_this_record(self):
	# 	hr_employee = self.env['hr.employee']
	# 	this_employee = hr_employee.search([('user_id','=',self.env.user.id)])

	# 	mapping_approved = self.env['itb.hr_approval_records']
	# 	mapping_records_counts = mapping_approved.search_count([('applicant_id','=',self.employee_id.id),('approver_id','=',this_employee[-1].id),('model','=','overtime'),('object_id','=',self.id)]) 
	# 	mapping_records_employee = mapping_approved.search([('applicant_id','=',self.employee_id.id),('approver_id','=',this_employee[-1].id),('model','=','overtime'),('object_id','=',self.id)])
		
	# 	self.should_approved = False
	# 	if mapping_records_counts > 0:
	# 		if mapping_records_employee[-1].sequence > 1:
	# 			previous_sequence_approver = mapping_records_employee[-1].sequence - 1
	# 			mapping_records_previous = mapping_approved.search([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id),('sequence','=',previous_sequence_approver)])
	# 			if mapping_records_previous[-1].is_approved == "approved" and mapping_records_employee[-1].is_approved == "pending":
	# 				self.should_approved = True
	# 		else:
	# 			if mapping_records_employee[-1].is_approved == "pending":
	# 				self.should_approved = True

	# def _search_approved_this_record(self,operator,value):
	# 	hr_employee = self.env['hr.employee']
	# 	this_employee = hr_employee.search([('user_id','=',self.env.user.id)])

	# 	mapping_approved = self.env['itb.hr_approval_records']
	# 	mapping_records_employee = mapping_approved.search([('approver_id','=',this_employee[-1].id),('model','=','overtime'),('is_approved','=','pending')])
	# 	mapping_records_counts = mapping_approved.search_count([('approver_id','=',this_employee[-1].id),('model','=','overtime'),('is_approved','=','pending')]) 

	# 	applicant_records = []
	# 	if mapping_records_counts > 0:
	# 		for request_travel in mapping_records_employee:
	# 			if request_travel.sequence > 1:
	# 				previous_sequence_approver = request_travel.sequence - 1
	# 				mapping_records_previous = mapping_approved.search([('model','=','overtime'),('object_id','=',request_travel.object_id.id),('sequence','=',previous_sequence_approver)])
	# 				if mapping_records_previous[-1].is_approved == "approved" and request_travel.is_approved == "pending":
	# 					#raise exceptions.ValidationError(request_travel.applicant_id.name)
	# 					applicant_records.append(request_travel.object_id)
	# 			else:
	# 				if request_travel.is_approved == "pending":
	# 					#raise exceptions.ValidationError(request_travel.applicant_id.name)
	# 					applicant_records.append(request_travel.object_id)

	# 	#raise exceptions.ValidationError(self.employee_id.id)
	# 	return [('id','in',applicant_records)]

	@api.one
	@api.constrains('start1','finish1','start2','finish2')
	def _start_date_no_more_then_finish_date(self):
		if self.start1 != False and self.finish1 != False and self.start2 != False and self.finish2 != False:
			format = '%Y-%m-%d %H:%M:%S'
			dummy_start1 = (datetime.strptime(self.start1,format)-timedelta(hours=17)).time()
			dummy_finish1 = (datetime.strptime(self.finish1,format)-timedelta(hours=17)).time()
			dummy_start2 = (datetime.strptime(self.start2,format)-timedelta(hours=17)).time()
			dummy_finish2 = (datetime.strptime(self.finish2,format)-timedelta(hours=17)).time()
			if dummy_start1 > dummy_finish1 or dummy_start2 > dummy_finish2:
				raise exceptions.ValidationError('finish date should be greather equal then start date')

	@api.one
	@api.constrains('start1','finish1','start2','finish2','day')
	def _overtime_request_not_intersected(self):
		overtime_request_model = self.env['itb.hr_overtime']
		overtime_request_thisemployee = overtime_request_model.search([('employee_id','=',self.employee_id.id),('day','=',self.day)])
		counter = 1
		if self.start1 != False and self.finish1 != False and self.start2 != False and self.finish2 != False:
			for overtime in overtime_request_thisemployee:
				if counter < len(overtime_request_thisemployee):
					format = '%Y-%m-%d %H:%M:%S'
					# Converted datetime for checked record start1-finish1 start2-finish2
					dummy_start1 = (datetime.strptime(self.start1,format)-timedelta(hours=17)).time()
					dummy_finish1 = (datetime.strptime(self.finish1,format)-timedelta(hours=17)).time()
					dummy_start2 = (datetime.strptime(self.start2,format)-timedelta(hours=17)).time()
					dummy_finish2 = (datetime.strptime(self.finish2,format)-timedelta(hours=17)).time()
					# Converted datetime for existing record start1-finish1 start2-finish2
					dummy_this_start1 = (datetime.strptime(overtime.start1,format)-timedelta(hours=17)).time()
					dummy_this_finish1 = (datetime.strptime(overtime.finish1,format)-timedelta(hours=17)).time()
					dummy_this_start2 = (datetime.strptime(overtime.start2,format)-timedelta(hours=17)).time()
					dummy_this_finish2 = (datetime.strptime(overtime.finish2,format)-timedelta(hours=17)).time()
					# Check intersection of start1-finish1 and start2-finish2 with existing data
					if (dummy_start1 <= dummy_this_finish1 and dummy_finish1 >= dummy_this_start1) or (dummy_start2 <= dummy_this_finish2 and dummy_finish2 >= dummy_this_start2):
						raise exceptions.ValidationError('Overtime Request intersected with another existing requests!')
				counter = counter + 1

	@api.one
	@api.constrains('start1','finish1','start2','finish2')
	def _overtime1_and_overtime2_not_intersected(self):
		if self.start1 != False and self.finish1 != False and self.start2 != False and self.finish2 != False:
			format = '%Y-%m-%d %H:%M:%S'
			# Converted datetime for checked record start1-finish1 start2-finish2
			dummy_start1 = (datetime.strptime(self.start1,format)-timedelta(hours=17)).time()
			dummy_finish1 = (datetime.strptime(self.finish1,format)-timedelta(hours=17)).time()
			dummy_start2 = (datetime.strptime(self.start2,format)-timedelta(hours=17)).time()
			dummy_finish2 = (datetime.strptime(self.finish2,format)-timedelta(hours=17)).time()
			# Check intersection between overtime1 dan overtime2
			if (dummy_start2 <= dummy_finish1) and (dummy_finish2 >= dummy_start1):
				raise exceptions.ValidationError('Start1-finish1 and start2-finish2 should not be intersected!')

	@api.one
	@api.constrains('start1','finish1','start2','finish2')
	def _overtime1_or_overtime2_not_null(self):
		if self.start1 == False and self.finish1 == False and self.start2 == False and self.finish2 == False:
			raise exceptions.ValidationError('Start1-finish1 and start2-finish2 must not be null!')
		else:
			if (self.start1 == False or self.finish1 == False) and (self.start2 == False or self.finish2 == False):
				raise exceptions.ValidationError('Start1-finish1 or Start2-finish2 must be completed or not at all!')

	@api.multi
	def action_state_draft(self):
		self.state = 'draft'
		# if self.state == 'confirmed':
		# 	hr_employee = self.env['hr.employee']
		# 	this_employee = hr_employee.search([('user_id','=',self.env.user.id)])

		# 	apprecords_model = self.env['itb.hr_approval_records']
		# 	apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('approver_id','=',this_employee[-1].id),('model','=','overtime'),('object_id','=',self.id)])
			
		# 	if len(apprecords_thisemployee) > 0:
		# 		if apprecords_thisemployee[-1].sequence > 1:
		# 			previous_sequence_approver = apprecords_thisemployee[-1].sequence - 1
		# 			apprecords_previous = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id),('sequence','=',previous_sequence_approver)])
		# 			if apprecords_thisemployee[-1].is_approved == "pending" and apprecords_previous[-1].is_approved == "approved":
		# 				all_apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)])
		# 				for apprecords in all_apprecords_thisemployee:
		# 					apprecords.write({'is_approved':'rejected'})
		# 				self.state = 'draft'
		# 			else:
		# 				raise exceptions.ValidationError('You are not authorized to abort this request!')
		# 		else:
		# 			if apprecords_thisemployee[-1].is_approved == "pending":
		# 				all_apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)])
		# 				for apprecords in all_apprecords_thisemployee:
		# 					apprecords.write({'is_approved':'rejected'})
		# 				self.state = 'draft'
		# 			else:
		# 				raise exceptions.ValidationError('You have already approved this request!')
		# 	else:
		# 		raise exceptions.ValidationError('You are not authorized to abort this request!')
		# else:
		# 	self.state = 'draft'

	@api.multi
	def action_state_confirm(self):
		self.state = 'confirmed'
		# apprecords_model = self.env['itb.hr_approval_records']
		# apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)]) 
		# apprecords_thisemployee_count = apprecords_model.search_count([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)]) 
		
		# # Cari trial request terakhir (apakah baru atau sudah ada)
		# latestTrial = 0
		# if apprecords_thisemployee_count > 0:
		# 	for apprecords in apprecords_thisemployee:
		# 		if (apprecords.trial > latestTrial):
		# 			latestTrial = apprecords.trial

		# # Tulis approval di daftar approval record dengan trial terbaru
		# if self.employee_id.user_id == self.env.user:
		# 	appmapping_model = self.env['itb.hr_request_approver']
		# 	approval_lists = appmapping_model.search([('applicant_id','=',self.employee_id.id)])
		# 	for approval in approval_lists:
		# 		apprecords_model.create({'applicant_id':self.employee_id.id,'approver_id':approval.approver_id.id,'sequence':approval.sequence,'model':'overtime','is_approved':'pending','object_id':self.id,'trial':latestTrial+1})
		# 	self.state = 'confirmed'
		# else:
		# 	raise exceptions.ValidationError('You are not authorized to confirm this request!')

	@api.multi
	def action_state_valid(self):
		self.state = 'valid'
		# apprecords_model = self.env['itb.hr_approval_records']
		# apprecords_thisemployee = apprecords_model.search([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)]) 
		# apprecords_counts = apprecords_model.search_count([('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)]) 
		
		# # Cari Di Mana Urutan Approver Saat Ini
		# sequence_approver_thisemployee = None
		# for apprecords in apprecords_thisemployee:
		# 	if apprecords.approver_id.user_id.id == self.env.user.id:
		# 		sequence_approver_thisemployee = apprecords.sequence

		# # Telusuri Approver Sebelumnya dan Cek Statusnya
		# approve_complete = True
		# if sequence_approver_thisemployee != None:
		# 	if sequence_approver_thisemployee > 1:
		# 		previous_sequence_approver = sequence_approver_thisemployee - 1
		# 		previous_approver_thisemployee = apprecords_model.search([('sequence','=',previous_sequence_approver),('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)])
		# 		if previous_approver_thisemployee[-1].is_approved == 'pending':
		# 			approve_complete = False
		# 			raise exceptions.ValidationError('You are not authorized to validate the overtime request!')
		# 		else:
		# 			this_approver_apprecords = apprecords_model.search([('sequence','=',sequence_approver_thisemployee),('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)])
		# 			this_approver_apprecords[-1].write({'is_approved':'approved'})
		# 	else:
		# 		if apprecords_counts > 1:
		# 			approve_complete = False
		# 		this_approver_apprecords = apprecords_model.search([('sequence','=',sequence_approver_thisemployee),('applicant_id','=',self.employee_id.id),('model','=','overtime'),('object_id','=',self.id)])
		# 		this_approver_apprecords[-1].write({'is_approved':'approved'})
		# else:
		# 	approve_complete = False
		# 	raise exceptions.ValidationError('You are not authorized to validate the overtime request!')

		# # Jika approval komplit dinyatakan valid (kurangi alokasi hari di leave allocation)
		# if approve_complete != True:
		# 	self.state = 'confirmed'
		# else:
		# 	self.state = 'valid'