from openerp import models, fields, api, exceptions

class Pangkat(models.Model):
	_name = 'itb.hr_pangkat'
	_rec_name = 'type_id'
	
	reference = fields.Char()
	start = fields.Date()
	finish = fields.Date()
	
	type_id =  fields.Many2one('itb.hr_pangkat_type', string='Pangkat')
	employee_id =  fields.Many2one('hr.employee', string='Name')

	
class Pangkat_Type(models.Model):
	_name = 'itb.hr_pangkat_type'
	
	name = fields.Char()
	code = fields.Char()