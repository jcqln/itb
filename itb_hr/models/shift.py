from openerp import models, fields, api, exceptions

class Shift(models.Model):
	_name = 'itb.hr_shift'
			
	name =  fields.Char(index=True)
	day = fields.Char()
	in_time = fields.Datetime()
	out_time = fields.Datetime()