from openerp import models, fields, api, exceptions,workflow
from lxml import etree

class Publication(models.Model):
	_name = 'itb.hr_publication'
	_inherit = ['ir.needaction_mixin']
	
	name = fields.Char()
	day = fields.Date(string='Date')
	publisher = fields.Char()
	state = fields.Selection([('draft','Draft'),('confirm','Confirmed'),('review','Reviewed'),('validate','Validated')], 'Status', default='draft',select=True, required=True, readonly=True, copy=False)
	media_id =  fields.Many2one('itb.hr_publication_media', string='Type')
	country_id =  fields.Many2one('res.country', string='Country', default='Indonesia')
	author_ids = fields.One2many('itb.hr_publication_author','publication_id',string='Authors')
	review_ids = fields.One2many('itb.hr_publication_review','publication_id',string='Reviews')
	
	
	@api.model
	def _needaction_domain_get(self):
		return [('state', '=', 'validate')]
	
	'''
	authors = fields.One2many('res.users',compute='_compute_authors',search='_search_authors',inverse='_write_authors')
	
	
	@api.multi
	def dumb(self,context):
		ctx = context
	    self.env['email.template'].browse(10).send_mail(self.id, force_send=True)
	    self.env['ir.actions.server'].browse(412).run()
		action = self.env['ir.actions.server'].browse(412)
		action.with_context(**ctx).run()
	
	
	@api.multi
	def confirm_publication(self):
		#action = self.env['ir.actions.server'].browse(412)
		#action.run()
		#self.state='confirm'
		#self.signal_workflow('submit_draft')
		workflow.trg_validate(self.env.uid, 'itb.hr_publication', self.id, 'submit_draft', self.cr)
	
	
	@api.model
	def fields_view_get(self, view_id=None, view_type='form',context=None, toolbar=False,submenu=False):
		res = super(Publication,self).fields_view_get(view_id=view_id, view_type=view_type, context=context,toolbar=toolbar, 
			submenu=submenu)
		doc = etree.XML(res['arch'])
		active_id = self.env.context.get('active_id', False)
		pub = self.env['itb.hr_publication'].browse(active_id)
		authors = pub.mapped('author_ids.employee_id.user_id.id')
		import pdb;pdb.set_trace()   
		if view_type == 'form':
			if self.env.user.id in authors and pub.state == 'draft':
				doc.set('create', 'true')
				doc.set('delete', 'true')
				doc.set('edit', 'true')
			else:
				doc.set('create', 'false')
				doc.set('delete', 'false')
				doc.set('edit', 'false')
		res['arch'] = etree.tostring(doc)
		return res
       
	'''
	
	@api.constrains('author_ids','review_ids')
	def check_author_not_reviewer(self):
		authors = self.mapped('author_ids.employee_id.id')
		reviewers = self.mapped('review_ids.employee_id.id') 
		
		if not set(authors).isdisjoint(reviewers):
			raise exceptions.ValidationError("Author can not be an reviewer at the same time for particular publication")
	
	
	def check_review_complete(self):
		score = self.mapped('review_ids.score')
		note = self.mapped('review_ids.note')
		
		if 0 not in score and False not in note:
			return True
		else:
			return False

			
class Publication_Media(models.Model):
	_name = 'itb.hr_publication_media'
	
	name = fields.Char()
	
	
class Publication_Author(models.Model):
	_name = 'itb.hr_publication_author'
	_rec_name = 'employee_id'
	
	sequence = fields.Integer()
	employee_id = fields.Many2one('hr.employee', string="Name")
	publication_id = fields.Many2one('itb.hr_publication',ondelete='cascade',required=True,index=True)
	research_group = fields.Char(related='employee_id.research_group_id.name',string='Research Group',store=True)
	
class Publication_Review(models.Model):
	_name = 'itb.hr_publication_review'
	
	score = fields.Integer()
	note = fields.Text()
	employee_id = fields.Many2one('hr.employee', string="Name")
	publication_id = fields.Many2one('itb.hr_publication',ondelete='cascade',required=True,index=True)