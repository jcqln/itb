from openerp import models, fields, api, exceptions

class Approval(models.Model):
	_name = 'itb.hr_approval'
	
	applicant_id =  fields.Many2one('hr.employee', string='Applicant')
	approver_id =  fields.Many2one('hr.employee', string='Approver')
	sequence = fields.Integer()
	model = fields.Selection([('leave','Leave'),('travel','Travel'),('overtime','Overtime')])
	object_id = fields.Integer()
	is_approved = fields.Selection([('pending','Pending'),('rejected','Rejected'),('approved','Approved')])
	trial = fields.Integer(default=1)