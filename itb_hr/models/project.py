from openerp import models, fields, api, exceptions

class Project(models.Model):
	_name = 'itb.hr_project'

	name = fields.Char()
	reference = fields.Char()
	start = fields.Date()
	finish = fields.Date()
	type_id =  fields.Many2one('itb.hr_project_type', string='Type')
	team_ids = fields.One2many('itb.hr_project_team','project_id',string='Team')
	
	
class Project_Type(models.Model):
	_name = 'itb.hr_project_type'
	
	name = fields.Char()
	
	
class Project_Team(models.Model):
	_name = 'itb.hr_project_team'
	
	employee_id = fields.Many2one('hr.employee', string='Name')
	role = fields.Selection([('member','Member'),('leader','Leader')],default='member')
	project_id = fields.Many2one('itb.hr_project',ondelete='cascade',required=True,index=True)