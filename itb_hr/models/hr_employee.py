from openerp import models, fields, api, exceptions

class Research_Group(models.Model):
	_name = 'itb.hr_research_group'
	
	name = fields.Char()



class hr_employee(models.Model):
	_inherit = 'hr.employee'
	
	pre_title = fields.Char()
	post_title = fields.Char()
	employment_type = fields.Selection([('itb','ITB'),('pns','PNS'),('cpns','CPNS'),('contract','Contract'),('outsource','Outsource')])
	nip_old = fields.Char()
	nip_new = fields.Char()
	nidn = fields.Char()
	nik = fields.Char()
	finger_id = fields.Char()
	employee_card = fields.Char()
	pns_start = fields.Date()
	cpns_start = fields.Date()
	birthplace = fields.Char()
	is_faculty = fields.Boolean()
	is_student = fields.Boolean()
	
	research_group_id = fields.Many2one('itb.hr_research_group', string="Research Group")
	education_ids = fields.One2many('itb.hr_education','employee_id',string='Educations')
	work_ids = fields.One2many('itb.hr_work','employee_id',string='Works')
	assignment_ids = fields.One2many('itb.hr_assignment','employee_id',string='Assignment')
	project_ids = fields.One2many('itb.hr_project_team','employee_id',string='Project')
	training_ids = fields.One2many('itb.hr_training','employee_id',string='Trainings')
	award_ids = fields.One2many('itb.hr_award','employee_id',string='Awards')
	membership_ids = fields.One2many('itb.hr_membership','employee_id',string='Membership')
	family_ids = fields.One2many('itb.hr_family','employee_id',string='Family')
	pangkat_ids = fields.One2many('itb.hr_pangkat','employee_id',string='Pangkat')
	jabatan_ids = fields.One2many('itb.hr_jabatan','employee_id',string='Jabatan')
	publication_ids = fields.One2many('itb.hr_publication_author','employee_id',string='Publication')