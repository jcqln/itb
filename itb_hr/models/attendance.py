from openerp import models, fields, api, exceptions
from datetime import datetime, timedelta

class Attendance(models.Model):
	_name = 'itb.hr_attendance'
	_rec_name = 'employee_id'
		
	employee_id =  fields.Many2one('hr.employee', string='Name',index=True)
	day = fields.Date(index=True, default="", readonly=True)
	in_time = fields.Datetime(default="", readonly=True)
	out_time = fields.Datetime(default="", readonly=True)
	actual_in_time = fields.Datetime(default="")
	actual_out_time = fields.Datetime(default="")
	latency_hour = fields.Float(default=0, readonly=True, compute='_set_latency_hour')
	overtime_hour = fields.Float(default=0, readonly=True, compute='_set_overtime_hour')
	is_attend = fields.Boolean(index=True, default=0, readonly=True, compute='_set_attendance_status')

	@api.multi
	def generate_calculation_record(self):

		@api.depends('employee_id')
		def _set_in_actual_value(self):
			employee = self.employee_id.search([('id','=',self.employee_id.id)])
			recordFingerPrint = self.env['itb.hr_hr_fingerprint'].search([('fingerprint_id','=',employee.finger_id)])
			thisEmployee = self.search([('employee_id','=',self.employee_id)])
			thisEmployee.write({'actual_in_time':recordFingerPrint[-1].time})
			#self.actual_in_time = recordFingerPrint[-1].time

		@api.depends('employee_id')
		def _set_out_actual_value(self):
			employee = self.employee_id.search([('id','=',self.employee_id.id)])
			recordFingerPrint = self.env['itb.hr_hr_fingerprint'].search([('fingerprint_id','=',employee.finger_id)])
			thisEmployee = self.search([('employee_id','=',self.employee_id)])
			thisEmployee.write({'actual_out_time':recordFingerPrint[0].time})
			#self.actual_out_time = recordFingerPrint[0].time

		@api.depends('in_time','actual_in_time')
		def _set_latency_hour(self):
			if self.in_time != "" and self.actual_in_time != "":
				differenceInterval = (self.actual_in_time - self.in_time).minutes
				if differenceInterval > 0:
					thisEmployee = self.search([('employee_id','=',self.employee_id)])
					thisEmployee.write({'latency_hour':differenceInterval})
					#self.latency_hour = differenceInterval

		@api.depends('out_time','actual_out_time','in_time','actual_in_time')
		def _set_overtime_hour(self):
			if self.out_time != "" and self.actual_out_time != "" and self.in_time != "" and self.actual_in_time != "":
				differenceActual = (self.actual_out_time - self.actual_in_time).minutes
				differenceScheduled = (self.out_time - self.in_time).minutes
				differenceInterval = differenceActual - differenceScheduled
				if differenceInterval > 0:
					thisEmployee = self.search([('employee_id','=',self.employee_id)])
					thisEmployee.write({'overtime_hour':differenceInterval})
					#self.overtime_hour = differenceInterval

		@api.depends('actual_in_time','actual_out_time','employee_id','day')
		def _set_attendance(self):
			thisEmployee = self.search([('employee_id','=',self.employee_id)])
			if self.actual_in_time != "" and self.actual_out_time != "":
				#self.is_attend = True
				thisEmployee.update({'is_attend':True})
			else:
				employee = self.employee_id.search([('id','=',self.employee_id.id)])
				travelRecord = self.env['itb.hr_hr_travel'].search([('employee_id','=',employee.id)])
				#self.is_attend = False
				thisEmployee.write({'is_attend':False})
				for travel in travelRecord:
					if travel.start >= self.day and travel.finish <= self.day:
						#self.is_attend = True
						thisEmployee.write({'is_attend':True})

	@api.one
	@api.depends('in_time','actual_in_time')
	def _set_latency_hour(self):
		self.latency_hour = 0
		if (self.in_time != False) and (self.actual_in_time != False):
			# Formatting in_time and actual_in_time from str to datetime
			format = '%Y-%m-%d %H:%M:%S'
			dummy_in_time = datetime.strptime(self.in_time,format)
			dummy_actual_in_time = datetime.strptime(self.actual_in_time,format)
			if (dummy_actual_in_time.time() > dummy_in_time.time()):
				# Get today in datetime format as before
				day = datetime.strptime(fields.Datetime.now(),format)
				# Calculate latency by combining date and time
				differenceIntervalLatency = (datetime.combine(day.date(),dummy_actual_in_time.time()) - datetime.combine(day.date(),dummy_in_time.time())).total_seconds() // 60
				self.latency_hour = differenceIntervalLatency

	@api.one
	@api.depends('in_time','actual_in_time','out_time','actual_out_time')
	def _set_overtime_hour(self):
		overtime_arrival = 0
		overtime_departure = 0
		overtime_request = self.env['itb.hr_overtime'].search([('employee_id','=',self.employee_id.id),('state','=','valid'),('day','=',self.day)])
		if (len(overtime_request) > 0):
			if (self.actual_out_time != False) and (self.actual_in_time != False) and (self.in_time != False) and (self.out_time != False):
				# Formatting in_time, out_time, actual_in_time, actual_out_time from str to datetime
				format = '%Y-%m-%d %H:%M:%S'
				day = datetime.strptime(fields.Datetime.now(),format)
				dummy_in_time = datetime.strptime(self.in_time,format)
				dummy_actual_in_time = datetime.strptime(self.actual_in_time,format)
				dummy_out_time = datetime.strptime(self.out_time,format)
				dummy_actual_out_time = datetime.strptime(self.actual_out_time,format)
				# Calculate overtime arrival (check if exist)
				dummy_overtime_arrival = (datetime.combine(day.date(),dummy_in_time.time()) - datetime.combine(day.date(),dummy_actual_in_time.time())).total_seconds() // 60
				if dummy_overtime_arrival > 0:
					overtime_arrival = dummy_overtime_arrival
				# Calculate overtime departure (check if exist)
				overtime_start2 = datetime.strptime(overtime_request[-1].start2,format)
				dummy_overtime_departure = (datetime.combine(day.date(),dummy_actual_out_time.time()) - datetime.combine(day.date(),overtime_start2.time())).total_seconds() // 60
				if dummy_overtime_departure > 0:
					overtime_departure = dummy_overtime_departure
		self.overtime_hour = overtime_arrival + overtime_departure

	@api.one
	@api.depends('actual_in_time','actual_out_time')
	def _set_attendance_status(self):
		self.is_attend = False
		if (self.actual_in_time != False) and (self.actual_out_time != False):
			self.is_attend = True

	@api.multi
	def refresh_attendance_record(self):
		# refresh data
		attendance = self.env['itb.hr_attendance'].search([])
		attendance.unlink()
		
		# format datetime
		format = '%Y-%m-%d %H:%M:%S'
		now = datetime.strptime(fields.Datetime.now(),format)
		# refill data with current data
		shiftRecordThisEmployee = self.env['itb.hr_shift_allocation'].search(['&', ('start', '<=', now.date()), ('finish', '>=', now.date())])

		for shiftRecord in shiftRecordThisEmployee:
			# if fields.Datetime.now() >= shiftRecord.start and fields.Datetime.now() <= shiftRecord.finish:
			# set status
			status = "normal"

			# set initial in_time and out_time from shift allocation
			employee_id = shiftRecord.employee_id.id
			day = datetime.strptime(fields.Datetime.now(),format)
			in_time = datetime.strptime(shiftRecord.shift_id.in_time,format)
			out_time = datetime.strptime(shiftRecord.shift_id.out_time,format)

			# set initial actual_in_time and actual_out_time from fingerprint
			# recordFingerPrintSum = self.env['itb.hr_fingerprint'].search_count([('fingerprint_id','=',employee_id)])
			actual_in_time = ""
			actual_out_time = ""
			# if recordFingerPrintSum > 0:
			# 	recordFingerPrint = self.env['itb.hr_fingerprint'].search([('fingerprint_id','=',employee_id)])
			# 	#day : 2016-04-11 05:44:21 
			# 	#recordFingerPrint[0].time.date() : 2016-04-10 10:20:00 
			# 	if datetime.strptime(recordFingerPrint[0].time,format).date() == day.date():	
			# 		actual_in_time = datetime.strptime(recordFingerPrint[0].time,format)
			# 	if datetime.strptime(recordFingerPrint[-1].time,format).date() == day.date():
			# 		actual_out_time = datetime.strptime(recordFingerPrint[-1].time,format)

			# set initial latency hour
			# latency_hour = 0
			# if actual_in_time != "":
			# 	# actual_in_time.time() : 09:00
			# 	# in_time.time() : 04:00 (harusnya 11:00)
			# 	dummy_in_time = in_time + timedelta(hours=7)
			# 	if actual_in_time.time() < dummy_in_time.time():
			# 		differenceIntervalLatency = (datetime.combine(day.date(),dummy_in_time.time()) - datetime.combine(day.date(),actual_in_time.time())).total_seconds() // 60
			# 		if differenceIntervalLatency > 0:
			# 			latency_hour = differenceIntervalLatency

			# set initial overtime hour
			# overtime_hour = 0
			# if actual_out_time != "" and actual_in_time != "":
			# 	differenceActual = (actual_out_time - actual_in_time).total_seconds() // 60
			# 	differenceScheduled = (out_time - in_time).total_seconds() // 60
			# 	differenceIntervalOvertime = differenceActual - differenceScheduled
			# 	if differenceIntervalOvertime > 0:
			# 		overtime_hour = differenceIntervalOvertime

			# set initial is_attend
			is_attend = False
			if actual_in_time != "" and actual_out_time != "":
				is_attend = True

			# check holiday schedule
			holidayRecordThisEmployee = self.env['itb.hr_holiday'].search([('employee_id','=',employee_id)])
			for holidayRecord in holidayRecordThisEmployee:
				if day == holidayRecord.day:
					in_time = ""
					out_time = ""
					status = "holiday"

			# check sickness
			sicknessRecordThisEmployee = self.env['itb.hr_sickness'].search([('employee_id','=',employee_id)])
			for sicknessRecord in sicknessRecordThisEmployee:
				if day >= sicknessRecord.start and day <= sicknessRecord.finish:
					in_time = ""
					out_time = ""
					status = "sick"

			# check travelling
			travellingRecordThisEmployee = self.env['itb.hr_travel'].search([('employee_id','=',employee_id)])
			for travellingRecord in travellingRecordThisEmployee:
				if fields.Datetime.now() >= travellingRecord.start and fields.Datetime.now() <= travellingRecord.finish:
					is_attend = True
					status = "travelling"

			# check overtime
			# overtimeRecordThisEmployee = self.env['itb.hr_overtime'].search([('employee_id','=',employee_id)])
			# for overtimeRecord in overtimeRecordThisEmployee:
			# 	if duration_minute1 > 0:
			# 		in_time -= duration_minute1
			# 	if duration_minute2 > 0:
			# 		out_time += duration_minute2

			# check leave allocation
			leaveRecordThisEmployee = self.env['itb.hr_leave'].search([('employee_id','=',employee_id)])
			for leaveRecord in leaveRecordThisEmployee:
				if fields.Datetime.now() >= leaveRecord.start and fields.Datetime.now() <= leaveRecord.finish:
					in_time = ""
					out_time = ""
					status = "leave"

			# write record in model
			attendance = self.env['itb.hr_attendance']
			if status == "travelling":
				attendance.create({'employee_id':employee_id,'day':day,'in_time':in_time,'out_time':out_time,'is_attend':is_attend})
			elif status != "holiday" and status != "sick" and status != "leave":
				if actual_out_time != "" and actual_in_time != "":
					attendance.create({'employee_id':employee_id,'day':day,'in_time':in_time,'out_time':out_time,'actual_in_time':actual_in_time,'actual_out_time':actual_out_time,'latency_hour':latency_hour,'overtime_hour':overtime_hour,'is_attend':is_attend})
				else:
					attendance.create({'employee_id':employee_id,'day':day,'in_time':in_time,'out_time':out_time,'is_attend':is_attend})