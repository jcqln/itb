from openerp import models, fields, api, exceptions

class Jabatan(models.Model):
	_name = 'itb.hr_jabatan'
	_rec_name = 'type_id'
	
	reference = fields.Char()
	start = fields.Date()
	finish = fields.Date()
	
	type_id =  fields.Many2one('itb.hr_jabatan_type', string='Jabatan')
	employee_id =  fields.Many2one('hr.employee', string='Name')

	
class Jabatan_Type(models.Model):
	_name = 'itb.hr_jabatan_type'
	
	name = fields.Char()