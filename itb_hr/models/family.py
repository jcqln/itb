from datetime import datetime
from openerp import models, fields, api, exceptions
from datetime import datetime

class Family(models.Model):
	_name = 'itb.hr_family'
	_rec_name = 'partner_id'

	def _set_current_employee(self):
		return self.env['hr.employee'].search([('user_id','=',self.env.user.id)])
	
	partner_id =  fields.Many2one('res.partner', string='Person Name')
	relation = fields.Selection([('spouse','Spouse'),('child','Child')],default="spouse")
	is_insured = fields.Boolean()
	sex = fields.Selection([('L','Laki-Laki'),('P','Perempuan')])
	birthdate = fields.Date()
	age = fields.Integer(compute='_set_age',store=True)
	state = fields.Selection([('draft','Draft'),('confirmed','Confirmed'),('valid','Validated')], 'Status', default='draft',select=True, required=True, readonly=True, copy=False)
	employee_id =  fields.Many2one('hr.employee', string='Name', default=_set_current_employee, readonly=True)

	@api.constrains('birthdate')
	def _birthdate_not_beyond_today(self):
		format = '%Y-%m-%d %H:%M:%S'
		format_date = '%Y-%m-%d'
		converted_birthdate_year = datetime.strptime(self.birthdate,format_date).date()
		converted_today = datetime.strptime(fields.Datetime.now(),format).date()
		if converted_birthdate_year > converted_today:
			raise exceptions.ValidationError('Birthdate must not exceed todays date!')

	@api.one
	@api.depends('birthdate')
	def _set_age(self):
		self.age = 0
		if self.birthdate != False:
			format = '%Y-%m-%d %H:%M:%S'
			format_date = '%Y-%m-%d'
			converted_birthdate_year = datetime.strptime(self.birthdate,format_date).year
			converted_today_year = datetime.strptime(fields.Datetime.now(),format).date().year
			if (converted_today_year - converted_birthdate_year) > 0:
				self.age = converted_today_year - converted_birthdate_year

	@api.multi
	def action_state_draft(self):
		self.state = 'draft'

	@api.multi
	def action_state_confirm(self):
		self.state = 'confirmed'

	@api.multi
	def action_state_valid(self):
		self.state = 'valid'