from openerp import models, fields, api, exceptions
from datetime import date
import math

class LeaveAllocation(models.Model):
	_name = 'itb.hr_leave_allocation'
	_rec_name = 'employee_id'
		
	employee_id =  fields.Many2one('hr.employee', string='Name',index=True)
	leave_type =  fields.Many2one('itb.hr_leave_type', string='Leave Type',index=True)
	day_amount = fields.Integer(compute='_set_day_amount',readonly=True)
	day_used = fields.Integer(default=0,readonly=True)
	day_saldo = fields.Integer(compute='_set_saldo_leave',readonly=True)
	year = fields.Integer(index=True)

	@api.one
	@api.depends('day_amount','day_used')
	def _set_saldo_leave(self):
		self.day_saldo = self.day_amount - self.day_used

	@api.one
	@api.depends('leave_type','employee_id','year')
	def _set_day_amount(self):
		this_leave_type = self.env['itb.hr_leave_type'].search([('id','=',self.leave_type.id)])
		if this_leave_type.is_annual != True:
			self.day_amount = this_leave_type.day_amount
		else:
			leave_allocation_model = self.env['itb.hr_leave_allocation']
			this_leave_allocation = leave_allocation_model.search([('employee_id','=',self.employee_id.id),('leave_type','=',self.leave_type.id),('year','<',self.year)])
			this_leave_allocationcount = leave_allocation_model.search_count([('employee_id','=',self.employee_id.id),('leave_type','=',self.leave_type.id),('year','<',self.year)])
			if this_leave_allocationcount > 0:
				self.day_amount = this_leave_type.day_amount + this_leave_allocation[-1].day_saldo
			else:
				self.day_amount = this_leave_type.day_amount

	@api.one
	@api.constrains('employee_id','year','leave_type')
	def _not_intersect_leave_same_employee(self):
		leave_model = self.env['itb.hr_leave_allocation']
		this_employee_leaves = leave_model.search([('employee_id','=',self.employee_id.id)])
		this_employee_leavescount = leave_model.search_count([('employee_id','=',self.employee_id.id)])
		counter = 1
		for leave in this_employee_leaves:
			if counter < this_employee_leavescount:
				if ((leave.year >= self.year) and (leave.leave_type == self.leave_type)):
					raise exceptions.ValidationError('The employee already has leave allocation on this type in this year!')
			counter = counter + 1

	@api.one
	@api.constrains('year')
	def _valid_year(self):
		if (self.year != 0):
			if ((int(math.log10(self.year))+1) == 4):
				if (self.year < date.today().year):
					raise exceptions.ValidationError('Year must be greater equal than current year')
			else:
				raise exceptions.ValidationError('Year must be in format yyyy')
		else:
			raise exceptions.ValidationError('Year must not be null')
			
		
	
class LeaveType(models.Model):
	_name = 'itb.hr_leave_type'
		
	name = fields.Char()
	is_annual = fields.Boolean(index=True)
	day_amount = fields.Integer()