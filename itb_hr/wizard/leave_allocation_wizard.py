from openerp import models, fields, api, exceptions
from datetime import date
import math

class LeaveAllocationWizard(models.TransientModel):
	_name = 'itb.hr_leave_allocation_wizard'
	year = fields.Integer(string='Allocating Year')
	employee_ids = fields.Many2many('hr.employee',relation='itb_hr_emp_leave_alct_wiz_rel',string='Choose List of Employees')

	def _leave_types_session(self):
		return self.env['itb.hr_leave_type'].browse(self._context.get('active_ids'))

	leave_type_ids = fields.Many2many('itb.hr_leave_type',relation='itb_hr_emp_leave_type_wiz_rel', string='Choose Leave Types',default=_leave_types_session)

	@api.one
	@api.constrains('year')
	def _valid_year(self):
		if (self.year != 0):
			if ((int(math.log10(self.year))+1) == 4):
				if (self.year < date.today().year):
					raise exceptions.ValidationError('Year must be greater equal than current year')
			else:
				raise exceptions.ValidationError('Year must be in format yyyy')
		else:
			raise exceptions.ValidationError('Year must not be null')

	@api.multi
	def do_mass_create_leave_allocation(self):
		self.ensure_one()
		if not (self.year or self.employee_ids or self.leave_type_ids):
			raise exceptions.ValidationError('No data to update!')

		employee_ids_leave = [employee.id for employee in self.employee_ids]
		leave_ids_type = [leave.id for leave in self.leave_type_ids]
		if ((len(employee_ids_leave)>0) and (len(leave_ids_type)>0)):
			for employee in employee_ids_leave:
				for leave in leave_ids_type:
					leave_allocation_model = self.env['itb.hr_leave_allocation']
					leave_allocation_model.create({'employee_id':employee,'leave_type':leave,'year':self.year})
		else:
			raise exceptions.ValidationError('Employee, Leave Type, or Year must not be null')
		


