from openerp import models, fields, api, exceptions

class ShiftAllocationWizard(models.TransientModel):
	_name = 'itb.hr_shift_allocation_wizard'

	def _shift_types_session(self):
		return self.env['itb.hr_shift'].browse(self._context.get('active_id'))

	shift_type_ids = fields.Many2one('itb.hr_shift',string='Choose Shift Type',default=_shift_types_session)
	start_shift = fields.Date()
	finish_shift = fields.Date()
	user_ids = fields.Many2many('hr.employee',string='Corresponding Employees')

	@api.multi
	def do_mass_create_shift(self):
		self.ensure_one()
		if not (self.start_shift or self.shift_type_ids or self.finish_shift or self.user_ids):
			raise exceptions.ValidationError('No data to update!')

		shift_types_ids = [shift.id for shift in self.shift_type_ids]
		shift_user_ids = [user.id for user in self.user_ids]
		if ((len(shift_types_ids)>0) and (len(shift_user_ids)>0) and (self.start_shift!=False) and (self.finish_shift!=False)):
			for shift_type in shift_types_ids:
				for user in shift_user_ids:
					shift_model = self.env['itb.hr_shift_allocation']
					shift_model.create({'employee_id':user,'shift_id':shift_type,'start':self.start_shift,'finish':self.finish_shift,'is_active':False})
		else:
			raise exceptions.ValidationError('Employee, Shift, or Date must not be null')
			