from openerp import models, fields, api, exceptions
from openerp import tools

class Asset(models.Model):
    _name = 'itb.asset'
    
    name = fields.Char()
    code = fields.Char()
    price = fields.Float()
    vendor_id = fields.Many2one('res.partner',index=True,required=True,string='Vendor')
    product_id = fields.Many2one('itb.asset_product',index=True,required=True,string='Catalog')
    location_id = fields.Many2one('itb.asset_location',index=True,required=True,string='Location')
    employee_id = fields.Many2one('hr.employee',index=True,string='Responsible')
    start = fields.Date()
    finish = fields.Date()
    start_warranty = fields.Date()
    finish_warranty = fields.Date()
    image = fields.Binary('Image')
    image_medium = fields.Binary(compute='_get_image', inverse='_set_image',search='_get_image',string='Medium-sized image',store=True)
    image_small = fields.Binary(compute='_get_image', inverse='_set_image',search='_get_image',string='Medium-sized image',store=True)
    
    
    @api.multi
    def _get_image(self, name, args):
        ids = self.ids
        result = dict.fromkeys(ids, False)
        for obj in self.browse(ids, context=self.env.context):
            result[obj.id] = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
        return result


    @api.multi
    def _set_image(self, name, value, args):
        self.ensure_one()
        id = self.id
        return self.write([id], {'image': tools.image_resize_image_big(value)}, context=self.env.context)