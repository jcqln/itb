from openerp import models, fields, api, exceptions

class Disposal(models.Model):
    _name = 'itb.asset_disposal'
    
    name = fields.Char()
    day = fields.Date(fields.Date.today())
    state = fields.Selection([('draft','Draft'),('confirm','Confirmed'),('validate','Validated')])
    line_ids = fields.One2many('itb.asset_disposal_line','disposal_id',string='Disposal Detail')
    
    
class Disposal_Line(models.Model):
    _name = 'itb.asset_disposal_line'
    
    disposal_id = fields.Many2one('itb.asset_disposal',ondelete='cascade',index=True,string='Disposal')
    asset_id = fields.Many2one('itb.asset',required=True,index=True,string='Asset')
    note = fields.Char()
        