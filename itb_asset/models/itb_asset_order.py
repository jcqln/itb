from openerp import models, fields, api, exceptions


class Order(models.Model):
    _name = 'itb.asset_order'
    
    name = fields.Char(index=True)
    type = fields.Selection([('breakdown','Breakdown'),('corrective','Corrective'),('preventive','Preventive'),('predictive','Predictive')])
    state = fields.Selection([('draft','Draft'),('released','Waiting Parts'),('ready','Ready to Maintenance'),('done','Done'),('cancel','Canceled')])
    problem = fields.Text()
    tool = fields.Text()
    labor = fields.Text()
    operation = fields.Text() 
    asset_id = fields.Many2one('itb.asset',required=True,index=True,string='Asset')
    task_id = fields.Many2one('itb.asset_task',required=True,index=True,string='Task')
    planned_time = fields.Datetime()
    scheduled_time = fields.Datetime()
    execution_time = fields.Datetime()
    source_document = fields.Char()