from openerp import models, fields, api, exceptions

class Allocation(models.Model):
    _name = 'itb.asset_allocation'
    
    name = fields.Char()
    day = fields.Date()
    state = fields.Selection([('draft','Draft'),('validate','Validated')])
    line_ids = fields.One2many('itb.asset_allocation_line','allocation_id',string='Allocation Detail')
    
    
class Allocation_Line(models.Model):
    _name = 'itb.asset_allocation_line'
    
    allocation_id = fields.Many2one('itb.asset_allocation',ondelete='cascade',index=True,string='Allocation')
    asset_id = fields.Many2one('itb.asset',required=True,index=True,string='Asset')
    location_id = fields.Many2one('itb.asset_location',index=True,string='Location')
    employee_id = fields.Many2one('hr.employee',index=True,string='Responsible')
    