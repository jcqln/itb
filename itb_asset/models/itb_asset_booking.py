from openerp import models, fields, api, exceptions

class Booking(models.Model):
    _name = 'itb.asset_booking'
    
    name = fields.Char()
    note = fields.Char()
    day = fields.Date(default=fields.Date.today())
    partner_id = fields.Many2one('res.partner',required=True,index=True,string='Responsible')
    state = fields.Selection([('draft','Draft'),('go','On Going'),('done','Done')])
    line_ids = fields.One2many('itb.asset_booking_line','booking_id',string='Booking Detail')
    
    
class Booking_Line(models.Model):
    _name = 'itb.asset_booking_line'
    
    booking_id = fields.Many2one('itb.asset_booking',ondelete='cascade',index=True,string='Booking')
    asset_id = fields.Many2one('itb.asset',index=True,string='Asset')
    start = fields.Datetime(default=fields.Datetime.now())
    finish = fields.Datetime()
    returned = fields.Date()
    
    
class Booking_Location(models.Model):
    _name = 'itb.asset_booking_location'
    
    name = fields.Char()
    note = fields.Char()
    day = fields.Date(default=fields.Date.today())
    partner_id = fields.Many2one('res.partner',required=True,index=True,string='Responsible')
    state = fields.Selection([('draft','Draft'),('go','On Going'),('done','Done')])
    line_ids = fields.One2many('itb.asset_booking_location_line','booking_location_id',string='Booking Location Detail')
    
    
class Booking_Location_Line(models.Model):
    _name = 'itb.asset_booking_location_line'
    
    booking_location_id = fields.Many2one('itb.asset_booking_location',ondelete='cascade',index=True,string='Booking Location')
    location_id = fields.Many2one('itb.asset_location',index=True,string='Location')
    start = fields.Datetime(default=fields.Datetime.now())
    finish = fields.Datetime()