from openerp import models, fields, api, exceptions


class Request(models.Model):
    _name = 'itb.asset_request'
    
    name = fields.Char(index=True)
    note = fields.Text()
    asset_id = fields.Many2one('itb.asset',index=True,required=True,string='Asset')
    breakdown = fields.Boolean()
    requested_time = fields.Datetime(default=fields.Datetime.now())
    execution_time = fields.Datetime()
    state = fields.Selection([('draft','Draft'),('confirm','Confirm'),('execute','Execution'),('done','Done')])    