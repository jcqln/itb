from openerp import models, fields, api, exceptions


class Task(models.Model):
    _name = 'itb.asset_task'
    
    name = fields.Char(index=True,required=True)
    category_id = fields.Many2one('itb.asset_category',required=True,index=True,string='Category')
    type = fields.Selection([('corrective','Corrective'),('preventive','Preventive'),('predictive','Predictive')])
    active = fields.Boolean(default=True)
    tools = fields.Text()
    labor = fields.Text()
    operation = fields.Text()    