from openerp import models, fields, api, exceptions

class Location(models.Model):
    _name = 'itb.asset_location'
    
    name = fields.Char(index=True)
    lecture_room = fields.Boolean()
    capacity = fields.Integer()
    parent_id = fields.Many2one('itb.asset_location',index=True,ondelete='restrict',string='Parent Location')
    child_ids = fields.One2many('itb.asset_location','parent_id',string='Child Location')
    