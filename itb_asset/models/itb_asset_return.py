from openerp import models, fields, api, exceptions

class Return(models.Model):
    _name = 'itb.asset_return'
    
    name = fields.Char()
    day = fields.Date(default=fields.Date.today())
    partner_id = fields.Many2one('res.partner',index=True,string='Responsible')
    booking_id = fields.Many2one('itb.asset_booking',index=True,string='Booking')
    line_ids = fields.One2many('itb.asset_return_line','return_id',string='Return Detail')
    
    
class Return_Line(models.Model):
    _name = 'itb.asset_return_line'
    
    return_id = fields.Many2one('itb.asset_return',ondelete='cascade',index=True,string='Return')
    asset_id = fields.Many2one('itb.asset',required=True,index=True,string='Asset')
    breakdown = fields.Boolean()
    note = fields.Char()        