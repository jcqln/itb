from openerp import models, fields, api, exceptions


class Measure(models.Model):
    _name = 'itb.asset_measure'
    
    name = fields.Char()
    uom = fields.Char()
    type = fields.Selection([('continues','Continues'),('gauge','Gauge')])


class Meter(models.Model):
    _name = 'itb.asset_meter'
    _rec_name = 'asset_id'
    
    asset_id = fields.Many2one('itb.asset',index=True,required=True,ondelete='cascade',string='Asset')
    measure_id = fields.Many2one('itb.asset_measure',index=True,required=True,ondelete='cascade')
    amount = fields.Float()