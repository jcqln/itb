from openerp import models, fields, api, exceptions

class Product(models.Model):
    _name = 'itb.asset_product'
    
    name = fields.Char(index=True,required=True)
    serial = fields.Char()
    specification = fields.Text(index=True)
    active = fields.Boolean(default=True)
    manufacturer_id = fields.Many2one('res.partner',string='Supplier',index=True)
    category_ids = fields.Many2many('itb.asset_category',string='Category')
    
    
    
class Category(models.Model):
    _name = 'itb.asset_category'
    
    name = fields.Char(index=True,required=True)
    product_ids = fields.Many2many('itb.asset_product',string='Product')